Note.js : Cuma Catetan Node.js
==============================

#### Sebelum mulai nyatet, sedikit introduction+curhat ya.

Jadi sekarang(pas gw nulis kalimat ini, bisa diliat history gitnya :v) gw udah sekitaran 3 tahun belajar PHP. 
Dan gw belajar PHP itu karena gw nggak sengaja sepik2 update status di facebook mau belajar web  
dan guru ICT di MAN gw komen nyaranin gw belajar framework Codeigniter(waktu itu masih versi 1.7an) sama Flex. 
Karena gw orangnya gampang penasaran ya gw baca-baca lah gw dokumentasi CI, awal-awal emang bisa dibilang hardcore banget belajarnya. 
Gimana nggak hardcore, waktu itu gw kuliah masih semester 2, baru 'dijejelin' VB, COBOL, sama pascal, 
ini sok2an nyicip framework OOP yang bahasa nativenya aja gw belum ngerti sama sekali waktu itu. 
Mungkin itu kenapa sampe sekarang gw agak susah move-on dari MVC dan susah banget mudeng mainin jeroan wordpress :v

Selesai curhat PHPnya, dan sekarang, lagi-lagi gw tertarik memperdalam sesuatu karena alesan yang sederhana.
Yap, sekarang gw lagi PDKT sama `Node.js`. Bukan karena gw ngebayangin gw bisa dapetin job banyak dari sini.
Tapi karena 'penasaran', yaudah cuma karena itu. Dan karena penasaran itu, gw coba-coba nekat buat tugas akhir gw pake teknologi 
si Node.js yang baru lahir beberapa tahun ini.

"Kenapa Node.js? kenapa bukan RoR, bukan microcontroller, bukan C++, java dsb?"

Gw juga kurang tau jelas alesannya. Mungkin karena ini yang paling memungkinkan sekarang, kalo dibilang mau belajar RoR, gw juga mau.
Microcontroller gw mau, memperdalam Java juga mau banget. 
Cuma saat ini, yang paling memungkinkan ya Node.js. Installnya gampang, cukup booming, resource cukup banyak. 
Dan mungkin juga gw tertarik karena dia `javascript`, dan agak 'freak' penggunaannya. 
Kayak yang gw bilang diatas, gw udah beberapa tahun ngandelin PHP untuk urusan server-side scripting, 
dan si Node.js ini konsepnya bisa dibilang agak jauh beda dari si PHP itu. Kenapa beda? apanya yang beda? dsb inilah yang mau gw catet disini.

#### > [PHP MENUJU NODE.JS](https://bitbucket.org/emsifa/catatan-belajar-node.js/src/master/php-ke-nodejs.md)

## Website Tutorial
- [learnallthenodes](http://www.learnallthenodes.com/) (article+screencast)
- [sailscasts](http://irlnathan.github.io/sailscasts/) (article+screencast) 
