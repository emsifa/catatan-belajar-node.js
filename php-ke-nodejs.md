DARI PHP MENUJU NODE.JS
=======================

Buat lu para PHP developer, terutama yang awam javascript kayak gw. Mungkin bakal mual2 kalo secara mendadak dijejelin project `node.js`.
Kenapa? nih dia alesannya:

#### Node.Js itu Non-Blocking (Asynchronous)
Apa maksudnya non-blocking? jadi di `node.js` ada banyak fungsi2 yang nggak bisa ngembaliin nilai secara langsung ke variable.
Misal katakanlah ada `fungsiA()` bersifat non-blocking dan `fungsiB(nilai_kembalian_dari_A)`, dalam kasus itu
`fungsiB` nggak bisa ada di blok yang sama dengan `fungsiA`, karena nilai kembalian dari `fungsiA` dikirim ke function yang disebut callback. Jadi kira2 PHP vs Node.js antara si fungsA dan B kayak gini

di PHP:
```php
$a = fungsiA();
$b = fungsiB($a);
```

di Node.js, 'kalau fungsiA bersifat non-blocking' nggak bisa begini:
```js
var a = fungsiA();
var b = fungsiB(a);
```
tapi mesti begini:
```js
fungsiA(function(a) { //<< fungsi ini disebut callback
	var b = fungsiB(a);
});
```

#### Callback Hell
Istilah [Callback Hell](http://callbackhell.com/) ini gw temuin sewaktu gw kelabakan nanganin sifat non-blockingnya `Node.js`. 
Jadi `callback hell` atau neraka callback ini bakal lu temuin sewaktu lu mulai serius di Node.js, neraka callback ini kondisi dimana kodinganlu mulai berantakan
dan terkesan condong ke-kanan karena fungsi2 yang bersifat non-blocking atau asynchronous tadi memaksa lu untuk masukin statement berikutnya di blok callback function. Dan nggak cuma kodingan yang jadi terkesan berantakan, tapi bakal ada kondisi dimana lu mesti pinter-pinter me-manage statement untuk ngehasilin sesuatu
sesuai harapan. 

Untuk cara ngatasin atau seenggaknya ngeringanin 'penderitaan' di neraka callback ini, ada beberapa cara yang bisa lu liat di link dibawah ini:

- [Callback Hell](http://callbackhell.com)
- [Strongloop: Managing Node.js Callback Hell with Promises, Generators and Other Approaches](http://strongloop.com/strongblog/node-js-callback-hell-promises-generators/)

#### Node.js Itu HTTP Server Sekaligus Server-Side Scriptingnya
Yang ini juga perlu jadi diketahui nih buat yang biasa main PHP. Kalo PHP kan yang berperan sebagai HTTP Servernya si `Apache`, `Nginx`, dsb.
Nah kalo `node.js` bisa dibilang dia yang berperan sebagai `PHP` + `Apache/Nginx`nya

Salah 1 perbedaan yang paling kerasa, misal nih, kalo di PHP lu nge-set global variable $foo = "foo", terus ada si A ngakses website lu, si $foo itu ya milik si A doang. Nah di node.js, saat lu ngeset var foo = "foo" di scope global, variable itu milik semua user yang terkoneksi ke server. 
Ini tuh useful + should be careful banget, misal dari situ lu bisa aja daftarin session langsung ke variable, nggak kayak PHP yang daftarin session ke file session_save_path(), cuma mungkin kalo nyimpen session ke variable bakal berat di memory server kalo yang ngakses membeludak.



#### Global Variable
Load module di Node.js kan pake fungsi `require()` tuh, awalnya gw kira `require()` di node.js itu kayak `require()` di PHP, jadi kalo di PHP kan misal di `file-a.php` lu buat variable `$foo = "foo"` terus `require("file-b.php")`, nah di `file-b.php` lu itu ada variable `$foo`. 

Nah di node.js, kalo mau share variable ke semua module yang di require, kita mesti taruh variable itu ke object `global`.

Jadi di node.js, nggak bisa begini:

```js
// file-a.js
var foo = "foo";
require("file-b");

// file-b.js
console.log(foo); // hasil: undefined
```

Tapi mesti begini:

```js
// file-a.js
var foo = "foo";
global.foo = foo;

require("file-b");

// file-b.js
console.log(foo); // hasil: "foo"
```